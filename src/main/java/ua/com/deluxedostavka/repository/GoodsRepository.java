package ua.com.deluxedostavka.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.com.deluxedostavka.entity.Goods;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Repository
public interface GoodsRepository extends JpaRepository<Goods,Long>,JpaSpecificationExecutor<Goods>{

    List<Goods> findAllBySubCategoryIdAndDeletedIsFalse(Long id);

    @Query("select g from Goods g join g.subCategory s join s.category c where c.id=:idCategory and g.deleted = false")
    List<Goods> findAllByCategory(@Param("idCategory") Long idCategory);

    Page<Goods> findAllByNameLikeAndDeletedIsFalse(String name, Pageable pageable);

    Page<Goods> findAllBySubCategoryIdAndDeletedIsFalse(Long id, Pageable pageable);

    Page<Goods> findAllBySubCategoryLinkValueAndDeletedIsFalse(String linkValue, Pageable pageable);

    @Query("select g from Goods g join g.subCategory s join s.category c where c.id=:idCategory and g.deleted = false")
    Page<Goods> findAllByCategoryIdAndDeletedIsFalse(@Param("idCategory") Long idCategory, Pageable pageable);

    @Query("select g from Goods g join g.subCategory s join s.category c where c.linkValue=:linkValue and g.deleted = false")
    Page<Goods> findAllByCategoryLinkValueAndDeletedIsFalse(@Param("linkValue") String linkValue, Pageable pageable);

    List<Goods> findAllBySaleIsTrueAndDeletedIsFalse();

    Goods findOneByLinkValue(String linkValue);

    List<Goods> findAllByDeletedIsFalse();

    Page<Goods> findAllByDeletedIsFalse(Pageable pageable);

    Optional<Goods> findByName(String name);
}
