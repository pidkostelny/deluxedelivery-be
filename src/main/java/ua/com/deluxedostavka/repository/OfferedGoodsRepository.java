package ua.com.deluxedostavka.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.com.deluxedostavka.entity.OfferedGoods;

import java.util.Optional;

@Repository
public interface OfferedGoodsRepository extends JpaRepository<OfferedGoods, Long> {

    Optional<OfferedGoods> findFirstByMainGoodsIdAndAnotherGoodsId(Long mainGoodsId, Long anotherGoodsId);

    OfferedGoods findOneByMainGoodsIdAndAnotherGoodsId(Long mainGoodsId, Long anotherGoodsId);
}
