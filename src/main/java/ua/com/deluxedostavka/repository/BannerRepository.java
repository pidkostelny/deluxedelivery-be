package ua.com.deluxedostavka.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.com.deluxedostavka.entity.Banner;

@Repository
public interface BannerRepository extends JpaRepository<Banner, Long> {
}
