package ua.com.deluxedostavka.service;

import ua.com.deluxedostavka.dto.category.CategoryRequest;
import ua.com.deluxedostavka.dto.category.CategoryResponse;
import ua.com.deluxedostavka.dto.category.CategoryWithChildrenResponse;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;

import java.io.IOException;
import java.util.List;

public interface CategoryService {

    List<CategoryResponse> getAll();

    CategoryResponse getOne(Long id);

    CategoryResponse create(CategoryRequest categoryRequest) throws IOException;

    CategoryResponse update(Long id,CategoryRequest categoryRequest) throws ObjectNotFoundException, IOException;

    void delete(Long id) throws ObjectNotFoundException;

    List<CategoryWithChildrenResponse> getAllTree();
}
