package ua.com.deluxedostavka.service;

import ua.com.deluxedostavka.dto.feedback.AdminFeedbackResponse;
import ua.com.deluxedostavka.dto.feedback.FeedbackRequest;
import ua.com.deluxedostavka.dto.feedback.FeedbackResponse;
import ua.com.deluxedostavka.dto.other.DataResponse;
import ua.com.deluxedostavka.dto.other.PaginationRequest;
import ua.com.deluxedostavka.entity.Feedback;

public interface FeedbackService {

    DataResponse<FeedbackResponse> getAll(PaginationRequest paginationRequest, Long goodsId);

    DataResponse<AdminFeedbackResponse> getFullAll(PaginationRequest paginationRequest, Long goodsId);

    FeedbackResponse getOne(Long idFeedback);

    AdminFeedbackResponse getFullOne(Long idFeedback);

    FeedbackResponse create(FeedbackRequest feedbackRequest);

    FeedbackResponse update(Long id, FeedbackRequest feedbackRequest);

    void delete(Long id);

    void delete(Feedback feedback);

}
