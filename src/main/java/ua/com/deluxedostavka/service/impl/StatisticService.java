package ua.com.deluxedostavka.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.com.deluxedostavka.dto.statistic.StatisticRequest;
import ua.com.deluxedostavka.dto.statistic.StatisticResponse;
import ua.com.deluxedostavka.entity.Order;
import ua.com.deluxedostavka.repository.OrderRepository;
import ua.com.deluxedostavka.specification.OrderSpecification;

import java.util.List;

@Service
public class StatisticService {

    @Autowired
    private OrderRepository orderRepository;

    public StatisticResponse getStatisticByFilter(StatisticRequest request) {
        List<Order> all = orderRepository.findAll(new OrderSpecification(request));
        final long totalSum = all.parallelStream().mapToLong(Order::getSum).reduce(0, Long::sum);
        final long avgSum = all.isEmpty() ? 0 : totalSum / all.size();
        return new StatisticResponse(totalSum, avgSum, all.size());
    }
}
