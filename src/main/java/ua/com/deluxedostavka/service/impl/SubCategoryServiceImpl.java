package ua.com.deluxedostavka.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.deluxedostavka.dto.subCategory.SubCategoryRequest;
import ua.com.deluxedostavka.dto.subCategory.SubCategoryResponse;
import ua.com.deluxedostavka.entity.Category;
import ua.com.deluxedostavka.entity.SeoInfo;
import ua.com.deluxedostavka.entity.SubCategory;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;
import ua.com.deluxedostavka.repository.CategoryRepository;
import ua.com.deluxedostavka.repository.SubCategoryRepository;
import ua.com.deluxedostavka.service.SubCategoryService;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubCategoryServiceImpl implements SubCategoryService {

    @Autowired
    private SubCategoryRepository subCategoryRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private FileService fileService;

    @Value("${subcategory.img.directory}")
    private String imageDirectoryPath;

    @Value("${subcategory.img.icon.directory}")
    private String iconDirectoryPath;

    @Override
    @Transactional
    public List<SubCategoryResponse> getAll() {
        return subCategoryRepository.findAll().stream().map(SubCategoryResponse::new).collect(Collectors.toList());
    }

    @Override
    public List<SubCategoryResponse> getAllByCategoryLinkValue(String linkValue) {
        return subCategoryRepository.findAllByCategoryLinkValue(linkValue).stream().map(SubCategoryResponse::new).collect(Collectors.toList());
    }

    @Override
    public List<SubCategoryResponse> getAllByCategoryId(Long categoryId) {
        return subCategoryRepository.findAllByCategoryId(categoryId).stream().map(SubCategoryResponse::new).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public SubCategoryResponse getOne(Long id) {
        return new SubCategoryResponse(subCategoryRepository.findOne(id));
    }

    @Override
    @Transactional
    public SubCategoryResponse create(SubCategoryRequest subCategoryRequest) throws ObjectNotFoundException, IOException {
        SubCategory subCategory = subCategoryRepository.saveAndFlush(subCategoryRequestToSubcategory(null, subCategoryRequest));
        return new SubCategoryResponse(subCategory);
    }

    @Override
    @Transactional
    public SubCategoryResponse update(Long id, SubCategoryRequest subCategoryRequest) throws ObjectNotFoundException, IOException {
        SubCategory subCategory = subCategoryRepository.saveAndFlush(subCategoryRequestToSubcategory(findSubCategory(id), subCategoryRequest));
        return new SubCategoryResponse(subCategory);
    }

    @Override
    public void delete(Long id) throws ObjectNotFoundException {
        SubCategory subCategory = findSubCategory(id);
        if (subCategory.getGoods().isEmpty()) {
            subCategoryRepository.delete(subCategory);
        } else {
            throw new IllegalArgumentException("Subcategory cannot be deleted");
        }
    }

    public SubCategory findSubCategory(Long id) throws ObjectNotFoundException {
        SubCategory subCategory = subCategoryRepository.findOne(id);
        if (subCategory != null) {
            return subCategory;
        } else {
            throw new ObjectNotFoundException("SubCategory with id :" + id + " not found");
        }
    }

    private SubCategory subCategoryRequestToSubcategory(SubCategory subCategory, SubCategoryRequest request) throws ObjectNotFoundException, IOException {
        if (subCategory == null) {
            subCategory = new SubCategory();
        }
        subCategory.setName(request.getName());
        subCategory.setLinkValue(request.getLinkValue());
        subCategory.setCategory(findCategory(request.getCategoryId()));
        SeoInfo seoInfo = new SeoInfo();
        seoInfo.setDescription(request.getSeoInfoRequest().getDescription());
        seoInfo.setHeader(request.getSeoInfoRequest().getHeader());
        seoInfo.setTitle(request.getSeoInfoRequest().getTitle());
        seoInfo.setTextContent(request.getSeoInfoRequest().getTextContent());
        subCategory.setSeoInfo(seoInfo);
        if (request.getImage() != null && !request.getImage().isEmpty()) {
            String imageName = fileService.saveFile(request.getImage(), imageDirectoryPath);
            subCategory.setPathToImage(imageName);
        }
        if (request.getIcon() != null && !request.getIcon().isEmpty()) {
            String iconName = fileService.saveFile(request.getIcon(), iconDirectoryPath);
            subCategory.setPathToIcon(iconName);
        }
        return subCategory;
    }

    private Category findCategory(Long id) throws ObjectNotFoundException {
        Category category = categoryRepository.findOne(id);
        if (category != null) {
            return category;
        } else {
            throw new ObjectNotFoundException("Category with id :" + id + " not found");
        }
    }


}
