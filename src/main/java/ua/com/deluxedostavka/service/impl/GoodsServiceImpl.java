package ua.com.deluxedostavka.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.deluxedostavka.dto.goods.FullAdminGoodsResponse;
import ua.com.deluxedostavka.dto.goods.FullGoodsResponse;
import ua.com.deluxedostavka.dto.goods.GoodsRequest;
import ua.com.deluxedostavka.dto.goods.GoodsResponse;
import ua.com.deluxedostavka.dto.other.DataResponse;
import ua.com.deluxedostavka.dto.other.PaginationRequest;
import ua.com.deluxedostavka.dto.other.SortRequest;
import ua.com.deluxedostavka.entity.Goods;
import ua.com.deluxedostavka.entity.SeoInfo;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;
import ua.com.deluxedostavka.repository.GoodsForOrderRepository;
import ua.com.deluxedostavka.repository.GoodsRepository;
import ua.com.deluxedostavka.service.FeedbackService;
import ua.com.deluxedostavka.repository.OrderRepository;
import ua.com.deluxedostavka.service.GoodsService;
import ua.com.deluxedostavka.service.SubCategoryService;
import ua.com.deluxedostavka.tools.Transliteration;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsRepository goodsRepository;

    @Autowired
    private SubCategoryService subCategoryService;

    @Autowired
    private FileService fileService;

    @Autowired
    private GoodsForOrderRepository goodsForOrderRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private FeedbackService feedbackService;

    @Value("${product.img.directory}")
    private String imageDirectoryPath;

    @Override
    public List<GoodsResponse> getAll() {
        return goodsRepository.findAllByDeletedIsFalse().stream().map(GoodsResponse::new).collect(Collectors.toList());
    }

    @Override
    public List<GoodsResponse> getAllBySubCategory(Long idSubCategory) {
        return goodsRepository.findAllBySubCategoryIdAndDeletedIsFalse(idSubCategory).stream().map(GoodsResponse::new).collect(Collectors.toList());
    }

    @Override
    public List<GoodsResponse> getAllByCategory(Long idCategory) {
        return goodsRepository.findAllByCategory(idCategory).stream().map(GoodsResponse::new).collect(Collectors.toList());
    }

    public List<GoodsResponse> getAllSales() {
        return goodsRepository.findAllBySaleIsTrueAndDeletedIsFalse().stream().map(GoodsResponse::new).collect(Collectors.toList());
    }

    @Override
    public DataResponse<GoodsResponse> getAll(PaginationRequest paginationRequest) {
        Page<Goods> page = goodsRepository.findAllByDeletedIsFalse(getPageRequest(paginationRequest));
        return new DataResponse<>(page.getContent().stream().map(GoodsResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    public DataResponse<GoodsResponse> getPageByCategoryId(Long id, PaginationRequest paginationRequest) {
        Page<Goods> page = goodsRepository.findAllByCategoryIdAndDeletedIsFalse(id, paginationRequest.toPageRequest());
        return new DataResponse<>(page.getContent().stream().map(GoodsResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    public DataResponse<GoodsResponse> getPageByCategoryLinkValue(String linkValue, PaginationRequest paginationRequest) {
        Page<Goods> page = goodsRepository.findAllByCategoryLinkValueAndDeletedIsFalse(linkValue, paginationRequest.toPageRequest());
        if (page.getTotalElements() == 0) {
            throw new IllegalArgumentException("No items for request");
        }
        return new DataResponse<>(page.getContent().stream().map(GoodsResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    public DataResponse<GoodsResponse> getPageBySubCategoryId(Long id, PaginationRequest paginationRequest) {
        Page<Goods> page = goodsRepository.findAllBySubCategoryIdAndDeletedIsFalse(id, paginationRequest.toPageRequest());
        return new DataResponse<>(page.getContent().stream().map(GoodsResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    public DataResponse<GoodsResponse> getPageBySubCategoryLinkValue(String linkValue, PaginationRequest paginationRequest) {
        Page<Goods> page = goodsRepository.findAllBySubCategoryLinkValueAndDeletedIsFalse(linkValue, paginationRequest.toPageRequest());
        if (page.getTotalElements() == 0) {
            throw new IllegalArgumentException("No items for request");
        }
        return new DataResponse<>(page.getContent().stream().map(GoodsResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    @Override
    public DataResponse<GoodsResponse> findByName(String name, PaginationRequest paginationRequest) {
        Page<Goods> page = goodsRepository.findAllByNameLikeAndDeletedIsFalse('%' + name + '%', getPageRequest(paginationRequest));
        return new DataResponse<>(page.getContent().stream().map(GoodsResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    @Override
    @Transactional(readOnly = true)
    public GoodsResponse getOne(Long idGoods) {
        return new GoodsResponse(goodsRepository.findOne(idGoods));
    }

    @Override
    @Transactional(readOnly = true)
    public FullGoodsResponse getFullOne(Long idGoods) {
        return new FullGoodsResponse(findOne(idGoods));
    }

    @Override
    @Transactional(readOnly = true)
    public FullGoodsResponse getFullOneByLinkValue(String linkValue) {
        return new FullGoodsResponse(findOneByLinkValue(linkValue));
    }

    @Override
    @Transactional(readOnly = true)
    public FullAdminGoodsResponse getAdminFullOne(Long idGoods) {
        return new FullAdminGoodsResponse(findOne(idGoods));
    }

    @Override
    @Transactional
    public GoodsResponse create(GoodsRequest goodsRequest) throws IOException, ObjectNotFoundException {
        return new GoodsResponse(goodsRepository.save(goodsRequestToGoods(null, goodsRequest)));
    }

    @Override
    @Transactional
    public GoodsResponse update(Long id, GoodsRequest goodsRequest) throws ObjectNotFoundException, IOException {
        return new GoodsResponse(goodsRepository.save(goodsRequestToGoods(findOne(id), goodsRequest)));
    }

    private Goods goodsRequestToGoods(Goods goods, GoodsRequest request) throws IOException, ObjectNotFoundException {
        if (goods == null) {
            goods = new Goods();
        }
        goods.setGoodsDescription(request.getDescription());
        goods.setPrice(request.getPrice());
        goods.setWeight(request.getWeight());

        goods.setLinkValue(generateLinkValue(goods, request));
        System.out.println(goods.getLinkValue());
        goods.setName(request.getName());
        goods.setDeleted(false);
        if (request.getSeoInfoRequest() != null) {
            SeoInfo seoInfo = new SeoInfo();
            seoInfo.setDescription(request.getSeoInfoRequest().getDescription());
            seoInfo.setHeader(request.getSeoInfoRequest().getHeader());
            seoInfo.setTitle(request.getSeoInfoRequest().getTitle());
            seoInfo.setTextContent(request.getSeoInfoRequest().getTextContent());
            goods.setSeoInfo(seoInfo);
        }
        goods.setSale(request.getSale() != null && request.getSale());
        if (request.getImg() != null && !request.getImg().isEmpty()) {
            String imageName = fileService.saveFile(request.getImg(), imageDirectoryPath);
            goods.setPathImage(imageName);
        }
        if (request.getSale() != null && request.getSale()) {
            goods.setSubCategory(null);
        } else {
            goods.setSubCategory(subCategoryService.findSubCategory(request.getSubCategoryId()));
        }
        return goods;
    }

    private String generateLinkValue(Goods goods, GoodsRequest request) {
        Optional<Goods> optionalGoods = goodsRepository.findByName(request.getName());

        if (!optionalGoods.isPresent() || (goods.getId() != null && goods.getId().equals(optionalGoods.get().getId()))) {
            return Transliteration.transliterateForUrl(request.getName());
        } else {
            return Transliteration.transliterateForUrl(request.getName()) + "-" + LocalDateTime.now().getSecond();
        }
    }

    @Override
    @Transactional
    public void delete(Long id) throws ObjectNotFoundException {
        Goods goods = findOne(id);
//        Set<Order> orders = new HashSet<>();
//        goods.getGoodsForOrders().forEach(goodsForOrder -> orders.add(goodsForOrder.getOrder()));
//        goodsForOrderRepository.delete(goods.getGoodsForOrders());
//        orderRepository.delete(orders);
//        if (goods.getPathImage() != null) {
//            Paths.get(System.getProperty("user.home"), imageDirectoryPath, goods.getPathImage()).toFile().delete();
//        }
//        goods.getFeedbacks().forEach(feedbackService::delete);
//        goodsRepository.delete(id);
        goods.setDeleted(true);
        goodsRepository.save(goods);
    }

    @Override
    public Goods findOne(Long id) throws ObjectNotFoundException {
        Goods goods = goodsRepository.findOne(id);
        if (goods != null) {
            return goods;
        } else {
            throw new ObjectNotFoundException("Goods with id : " + id + " not found !!!");
        }
    }

    private Goods findOneByLinkValue(String linkValue) throws ObjectNotFoundException {
        Goods goods = goodsRepository.findOneByLinkValue(linkValue);
        if (goods != null) {
            return goods;
        } else {
            throw new ObjectNotFoundException("Goods with link value : " + linkValue + " not found !!!");
        }
    }

    private PageRequest getPageRequest(PaginationRequest paginationRequest) {
        SortRequest sortRequest = paginationRequest.getSort();
        PageRequest pageRequest;
        if (sortRequest == null) {
            pageRequest = new PageRequest(paginationRequest.getPage(), paginationRequest.getSize());
        } else {
            pageRequest = new PageRequest(paginationRequest.getPage(), paginationRequest.getSize(), new Sort(sortRequest.getDirection(), sortRequest.getFieldName()));
        }
        return pageRequest;
    }


}
