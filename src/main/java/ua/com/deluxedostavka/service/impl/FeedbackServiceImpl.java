package ua.com.deluxedostavka.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.deluxedostavka.dto.feedback.AdminFeedbackResponse;
import ua.com.deluxedostavka.dto.feedback.FeedbackRequest;
import ua.com.deluxedostavka.dto.feedback.FeedbackResponse;
import ua.com.deluxedostavka.dto.other.DataResponse;
import ua.com.deluxedostavka.dto.other.PaginationRequest;
import ua.com.deluxedostavka.dto.other.SortRequest;
import ua.com.deluxedostavka.entity.Feedback;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;
import ua.com.deluxedostavka.repository.FeedbackRepository;
import ua.com.deluxedostavka.service.FeedbackService;
import ua.com.deluxedostavka.service.GoodsService;

import java.util.stream.Collectors;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private GoodsService goodsService;

    @Override
    @Transactional(readOnly = true)
    public DataResponse<FeedbackResponse> getAll(PaginationRequest paginationRequest, Long goodsId) {
        Page<Feedback> page = getFeedbacksPage(paginationRequest, goodsId);
        return new DataResponse<>(page.getContent().stream().map(FeedbackResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    @Override
    @Transactional(readOnly = true)
    public DataResponse<AdminFeedbackResponse> getFullAll(PaginationRequest paginationRequest, Long goodsId) {
        Page<Feedback> page = getFeedbacksPage(paginationRequest, goodsId);
        return new DataResponse<>(page.getContent().stream().map(AdminFeedbackResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    private Page<Feedback> getFeedbacksPage(PaginationRequest paginationRequest, Long goodsId) {
        Page<Feedback> page;
        if (goodsId != null) {
            page = feedbackRepository.findAllByGoodsId(goodsId, getPageRequest(paginationRequest));
        } else {
            page = feedbackRepository.findAll(getPageRequest(paginationRequest));
        }
        return page;
    }

    @Override
    @Transactional(readOnly = true)
    public FeedbackResponse getOne(Long idFeedback) {
        return new FeedbackResponse(feedbackRepository.findOne(idFeedback));
    }

    @Override
    @Transactional(readOnly = true)
    public AdminFeedbackResponse getFullOne(Long idFeedback) {
        return new AdminFeedbackResponse(feedbackRepository.findOne(idFeedback));
    }

    @Override
    @Transactional
    public FeedbackResponse create(FeedbackRequest feedbackRequest) {
        return new FeedbackResponse(feedbackRepository.save(feedbackRequestToFeedback(null, feedbackRequest)));
    }


    @Override
    @Transactional
    public FeedbackResponse update(Long id, FeedbackRequest feedbackRequest) {
        return new FeedbackResponse(feedbackRepository.save(feedbackRequestToFeedback(findOne(id), feedbackRequest)));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        delete(findOne(id));
    }

    @Override
    @Transactional
    public void delete(Feedback feedback) {
        feedbackRepository.delete(feedback);
    }

    private Feedback feedbackRequestToFeedback(Feedback feedback, FeedbackRequest request) {
        if (feedback == null) {
            feedback = new Feedback();
        }
        feedback.setName(request.getName());
        feedback.setMessageText(request.getMessage());
        feedback.setPhoneNumber(request.getPhoneNumber());
        feedback.setRating(request.getRating());
        feedback.setGoods(goodsService.findOne(request.getGoodsId()));
        return feedback;
    }

    private Feedback findOne(Long id) {
        Feedback feedback = feedbackRepository.findOne(id);
        if (feedback != null) {
            return feedback;
        } else {
            throw new ObjectNotFoundException("Feedback with id : " + id + " not found !!!");
        }
    }

    private PageRequest getPageRequest(PaginationRequest paginationRequest) {
        SortRequest sortRequest = paginationRequest.getSort();
        PageRequest pageRequest;
        if (sortRequest == null) {
            pageRequest = new PageRequest(paginationRequest.getPage(), paginationRequest.getSize());
        } else {
            pageRequest = new PageRequest(paginationRequest.getPage(), paginationRequest.getSize(), new Sort(sortRequest.getDirection(), sortRequest.getFieldName()));
        }
        return pageRequest;
    }

}
