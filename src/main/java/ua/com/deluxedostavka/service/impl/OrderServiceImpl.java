package ua.com.deluxedostavka.service.impl;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.deluxedostavka.dto.goods.GoodsForOrderRequest;
import ua.com.deluxedostavka.dto.goods.GoodsForOrderResponse;
import ua.com.deluxedostavka.dto.order.OrderRequest;
import ua.com.deluxedostavka.dto.order.OrderResponse;
import ua.com.deluxedostavka.dto.order.OrderSearchRequest;
import ua.com.deluxedostavka.dto.other.DataResponse;
import ua.com.deluxedostavka.dto.other.PaginationRequest;
import ua.com.deluxedostavka.dto.other.SortRequest;
import ua.com.deluxedostavka.dto.user.UserRequest;
import ua.com.deluxedostavka.dto.user.UserResponse;
import ua.com.deluxedostavka.entity.*;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;
import ua.com.deluxedostavka.exception.OrderException;
import ua.com.deluxedostavka.repository.GoodsForOrderRepository;
import ua.com.deluxedostavka.repository.GoodsRepository;
import ua.com.deluxedostavka.repository.OrderRepository;
import ua.com.deluxedostavka.repository.UserRepository;
import ua.com.deluxedostavka.service.OrderService;
import ua.com.deluxedostavka.specification.OrderSpecification;
import ua.com.deluxedostavka.tools.SmsSender;
import ua.com.deluxedostavka.tools.TelegramTool;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static ua.com.deluxedostavka.tools.Constants.TELEGRAM_ORDERS_URL;

@Service
@Log
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GoodsRepository goodsRepository;

    @Autowired
    private GoodsForOrderRepository goodsForOrderRepository;

    @Autowired
    private SmsSender smsSender;

    @Autowired
    private TelegramTool telegramTool;


    @Override
    @Transactional
    public OrderResponse order(OrderRequest orderRequest) throws ObjectNotFoundException {
        log.info("Order : " + orderRequest.getUser().getNumber());
        Order order = new Order();
        order.setDescription(orderRequest.getDescription());
        order.setArchive(false);
        order.setDontCallBack(orderRequest.getDontCallBack() != null ? orderRequest.getDontCallBack() : false);
        order.setDontNeedCutlery(orderRequest.getDontNeedCutlery() != null ? orderRequest.getDontNeedCutlery() : false);
        order.setTime(LocalTime.now(ZoneId.systemDefault()));
        order.setDate(LocalDate.now(ZoneId.systemDefault()));
        order.setPaymentType(orderRequest.getPaymentType() != null ? orderRequest.getPaymentType() : PaymentType.CASH);
        order = orderRepository.saveAndFlush(order);
        Long sum = createGoodsForOrder(orderRequest.getGoods(), order);
        sum += orderRequest.getDeliveryPrice();
        order.setDeliveryPrice(orderRequest.getDeliveryPrice());
        order.setSum(sum);
        sum /= 100;
        sum += 15;//add price of box
//        smsSender.sendSms(orderRequest.getUser().getName(),orderRequest.getUser().getNumber(),sum);
        order.setUser(createUser(orderRequest.getUser(), order));//test
        order = orderRepository.saveAndFlush(order);
        order.setGoodsForOrders(goodsForOrderRepository.findByOrderId(order.getId()));
        OrderResponse orderResponse = new OrderResponse(order);
        telegramTool.sendNotification(TELEGRAM_ORDERS_URL + createMessageForTelegram(orderResponse, sum));
        return orderResponse;
    }

    private String createMessageForTelegram(OrderResponse orderResponse, Long sum) {
        String text = "Нове замовлення : \n";
        UserResponse user = orderResponse.getUserResponse();
        text += "Ім'я замовника : " + user.getName() + "\n";
        text += "Номер телефону : 0" + user.getNumber() + "\n";
        text += "Адреса доставки : " + user.getAddress() + " " + user.getAddressNumber() + "\n";
        text += "Передзвонити : " + (orderResponse.getDontCallBack() ? "Ні" : "Так") + "\n";
        text += "Чи потрібні прибори : " + (orderResponse.getDontNeedCutlery() ? "Ні" : "Так") + "\n";
        text += "Коментар : " + orderResponse.getDescription() + "\n";
        text += "Дата/час : " + orderResponse.getDate() + "\n";
        if (orderResponse.getPaymentType().equals(PaymentType.CASH)) {
            text += "Тип оплати : ГОТІВКА \n";
        } else if (orderResponse.getPaymentType().equals(PaymentType.TERMINAL)) {
            text += "Тип оплати : КАРТКОЮ ЧЕРЕЗ ТЕРМІНАЛ \n";
        }
        text += "Загальна сума замовлення : " + sum + "\n";
        text += "Сума доставки : " + orderResponse.getDeliveryPrice() / 100 + "\n";
        String dishes = "\n";
        for (GoodsForOrderResponse goodsResponseList : orderResponse.getGoodsResponseList()) {
            dishes += "Назва: " + goodsResponseList.getName() + " Ціна : " + (goodsResponseList.getPrice() / 100) + " Кількість : " + goodsResponseList.getCount() + "\n";
            dishes += "------------------------------------------------------\n";
        }
        dishes += "";
        text += "Замовлення : \nУпаковака 15 грн\n" + dishes;
        return text;
    }

    @Override
    @Transactional
    public OrderResponse updateOrder(OrderRequest orderRequest) throws ObjectNotFoundException, OrderException {
        Order order = orderRepository.findOne(orderRequest.getId());
        if (order != null) {
            goodsForOrderRepository.delete(order.getGoodsForOrders());
            Long sum = createGoodsForOrder(orderRequest.getGoods(), order);
            order.setSum(sum);
            return new OrderResponse(order);
        } else {
            throw new OrderException("Order for update not found");
        }
    }

    @Override
    @Transactional
    public List<OrderResponse> getAll() {
        return orderRepository.findAll().stream().map(OrderResponse::new).collect(Collectors.toList());
    }

    @Override
    public OrderResponse archived(Long id) {
        Order order = orderRepository.findOne(id);
        order.setArchive(true);
        return new OrderResponse(orderRepository.saveAndFlush(order));
    }

    @Override
    public OrderResponse unarchived(Long id) {
        Order order = orderRepository.findOne(id);
        order.setArchive(false);
        return new OrderResponse(orderRepository.saveAndFlush(order));
    }

    @Override
    public DataResponse<OrderResponse> getAll(PaginationRequest paginationRequest) {
        Page<Order> page = orderRepository.findAll(getPageRequest(paginationRequest));
        return new DataResponse<>(page.getContent().stream().map(OrderResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    @Override
    public DataResponse<OrderResponse> getAllByArchived(PaginationRequest paginationRequest, Boolean archived) {
        Page<Order> page = orderRepository.findByArchive(archived, getPageRequest(paginationRequest));
        return new DataResponse<>(page.getContent().stream().map(OrderResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    @Override
    public void delete(Long id) {
        orderRepository.delete(id);
    }

    @Override
    public DataResponse<OrderResponse> searchOrders(String value, Integer page, Integer size, String sortBy, Sort.Direction direction) {
        PageRequest pageRequest;
        if (sortBy != null && direction != null) {
            Sort sort = new Sort(direction, sortBy);
            pageRequest = new PageRequest(page, size, sort);
        } else {
            pageRequest = new PageRequest(page, size);
        }
        Page<Order> orderPage = orderRepository.findByUserNameOrUserSurNameOrUserNumber(value, pageRequest);
        return new DataResponse<>(orderPage.getContent().stream().map(OrderResponse::new).collect(Collectors.toList()), orderPage.getTotalElements(), orderPage.getTotalPages());
    }

    public DataResponse<OrderResponse> searchOrders(OrderSearchRequest request) {
        Page<Order> page = orderRepository.findAll(new OrderSpecification(request), request.getPaginationRequest().toPageRequest());
        return new DataResponse<>(page.getContent().stream().map(OrderResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    private PageRequest getPageRequest(PaginationRequest paginationRequest) {
        SortRequest sortRequest = paginationRequest.getSort();
        PageRequest pageRequest;
        if (sortRequest == null) {
            pageRequest = new PageRequest(paginationRequest.getPage(), paginationRequest.getSize());
        } else {
            pageRequest = new PageRequest(paginationRequest.getPage(), paginationRequest.getSize(), new Sort(sortRequest.getDirection(), sortRequest.getFieldName()));
        }
        return pageRequest;
    }

    private Long createGoodsForOrder(List<GoodsForOrderRequest> goodsForOrderRequestList, Order order) throws ObjectNotFoundException {
        AtomicReference<Long> sum = new AtomicReference<>(0L);
        Map<Category, Long> goodsInCategoriesCount = new HashMap<>();

        goodsForOrderRequestList.forEach(request -> {
            Goods goods = findOne(request.getIdGoods());
            Category category = goods.getSubCategory().getCategory();
            goodsInCategoriesCount.merge(category, request.getCount(), Long::sum);
            GoodsForOrder goodsForOrder = new GoodsForOrder();
            goodsForOrder.setCount(request.getCount());
            sum.updateAndGet(v -> v + (goods.getPrice() * request.getCount()));
            goodsForOrder.setGoods(goods);
            goodsForOrder.setOrder(order);
            goodsForOrderRepository.saveAndFlush(goodsForOrder);
        });
        goodsInCategoriesCount.forEach(((category, goodsCount) -> {
            if (category.getMinGoodsPerOrder() != null && category.getMinGoodsPerOrder() > goodsCount) {
                throw new OrderException("Min count of goods in category: " + category.getId() + " is " + category.getMinGoodsPerOrder());
            }
        }));
        return sum.get();
    }

    private User createUser(UserRequest userRequest, Order order) {
        User user = new User();
        user.setName(userRequest.getName());
        user.setSurName(userRequest.getSurName());
        user.setAddress(userRequest.getAddress());
        user.setAddressNumber(userRequest.getAddressNumber());
        user.setNumber(userRequest.getNumber());
        user.setOrder(order);
        return userRepository.saveAndFlush(user);
    }

    private Goods findOne(Long id) throws ObjectNotFoundException {
        Goods goods = goodsRepository.findOne(id);
        if (goods != null) {
            return goods;
        } else {
            throw new ObjectNotFoundException("Goods with id : " + id + " not found !!!");
        }
    }

}
