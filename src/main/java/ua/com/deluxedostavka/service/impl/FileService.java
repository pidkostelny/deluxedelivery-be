package ua.com.deluxedostavka.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.UUID;

@Service
public class FileService {

    @Value("${img.base-directory}")
    private String imgBaseDirectory;

    public String saveFile(String image, String directory) throws IOException {
        Path pathToDirectory = getPathToDirectory(directory);

        String[] data = image.split(",");
        String metaInfo = data[0];
        String base64File = data[1];

        String fileName = createFileName(getFileExtensionFromMetaInfo(metaInfo));

        Files.write(
                Paths.get(pathToDirectory.toString(), fileName),
                Base64.getDecoder().decode(base64File.getBytes())
        );
        return fileName;
    }

    private String createFileName(String fileExtension) {
        String fileName = UUID.randomUUID().toString();
        return String.format("%s.%s", fileName, fileExtension);
    }

    private String getFileExtensionFromMetaInfo(String metaInfo) {
        return metaInfo.split("/")[1].split(";")[0];
    }

    private Path getPathToDirectory(String directory) throws IOException {
        Path path = Paths.get(imgBaseDirectory, directory);
        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }
        return path;
    }
}
