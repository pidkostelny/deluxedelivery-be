package ua.com.deluxedostavka.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.deluxedostavka.dto.offeredGoods.OfferedGoodsRequest;
import ua.com.deluxedostavka.entity.Goods;
import ua.com.deluxedostavka.entity.OfferedGoods;
import ua.com.deluxedostavka.exception.ObjectAlreadyExistException;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;
import ua.com.deluxedostavka.repository.OfferedGoodsRepository;
import ua.com.deluxedostavka.service.GoodsService;
import ua.com.deluxedostavka.service.OfferedGoodsService;

@Service
public class OfferedGoodsServiceImpl implements OfferedGoodsService {

    @Autowired
    private OfferedGoodsRepository offeredGoodsRepository;

    @Autowired
    private GoodsService goodsService;

    @Override
    @Transactional
    public void create(OfferedGoodsRequest offeredGoodsRequest) {
        if (offeredGoodsRepository
                .findFirstByMainGoodsIdAndAnotherGoodsId(offeredGoodsRequest.getMainGoodsId(), offeredGoodsRequest.getAnotherGoodsId())
                .isPresent()) {
            throw new ObjectAlreadyExistException("Product " + offeredGoodsRequest.getAnotherGoodsId() + " already offered for product " + offeredGoodsRequest.getMainGoodsId());
        }
        Goods mainGoods = goodsService.findOne(offeredGoodsRequest.getMainGoodsId());
        Goods anotherGoods = goodsService.findOne(offeredGoodsRequest.getAnotherGoodsId());

        OfferedGoods offeredGoods = new OfferedGoods();
        offeredGoods.setMainGoods(mainGoods);
        offeredGoods.setAnotherGoods(anotherGoods);
        offeredGoodsRepository.save(offeredGoods);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        OfferedGoods offeredGoods = findOneById(id);
        offeredGoodsRepository.delete(offeredGoods);
    }

    @Override
    @Transactional
    public void delete(OfferedGoodsRequest offeredGoodsRequest) {
        OfferedGoods offeredGoods = offeredGoodsRepository.findOneByMainGoodsIdAndAnotherGoodsId(offeredGoodsRequest.getMainGoodsId(), offeredGoodsRequest.getAnotherGoodsId());
        if (offeredGoods != null) {
            offeredGoodsRepository.delete(offeredGoods);
        } else {
            throw new ObjectNotFoundException("Offered goods with id :" + offeredGoodsRequest.getAnotherGoodsId()
                    + " for goods with id " + offeredGoodsRequest.getMainGoodsId() + " not found");
        }
    }

    private OfferedGoods findOneById(Long id) throws ObjectNotFoundException {
        OfferedGoods offeredGoods = offeredGoodsRepository.findOne(id);
        if (offeredGoods != null) {
            return offeredGoods;
        } else {
            throw new ObjectNotFoundException("Offered goods with id :" + id + " not found");
        }
    }
}
