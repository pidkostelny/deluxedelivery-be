package ua.com.deluxedostavka.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.deluxedostavka.dto.category.CategoryRequest;
import ua.com.deluxedostavka.dto.category.CategoryResponse;
import ua.com.deluxedostavka.dto.category.CategoryWithChildrenResponse;
import ua.com.deluxedostavka.entity.Category;
import ua.com.deluxedostavka.entity.SeoInfo;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;
import ua.com.deluxedostavka.repository.CategoryRepository;
import ua.com.deluxedostavka.service.CategoryService;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private FileService fileService;

    @Value("${category.img.directory}")
    private String imageDirectoryPath;

    @Value("${category.img.icon.directory}")
    private String iconDirectoryPath;

    @Override
    @Transactional(readOnly = true)
    public List<CategoryResponse> getAll() {
        return categoryRepository.findAll().stream().map(CategoryResponse::new).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public CategoryResponse getOne(Long id) {
        return new CategoryResponse(categoryRepository.findOne(id));
    }

    @Override
    @Transactional
    public CategoryResponse create(CategoryRequest categoryRequest) throws IOException {
        Category category = categoryRepository.saveAndFlush(categoryRequestToCategory(null, categoryRequest));
        return new CategoryResponse(category);
    }

    @Override
    @Transactional
    public CategoryResponse update(Long id, CategoryRequest categoryRequest) throws ObjectNotFoundException, IOException {
        Category category = categoryRepository.saveAndFlush(categoryRequestToCategory(findCategory(id), categoryRequest));
        return new CategoryResponse(category);
    }

    @Override
    @Transactional
    public void delete(Long id) throws ObjectNotFoundException {
        Category category = findCategory(id);
        if (category.getSubCategoryList().isEmpty()) {
            categoryRepository.delete(category);
        } else {
            throw new IllegalArgumentException("Category cannot be deleted");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<CategoryWithChildrenResponse> getAllTree() {
        return categoryRepository.findAll().stream().map(CategoryWithChildrenResponse::new).collect(Collectors.toList());
    }

    private Category categoryRequestToCategory(Category category, CategoryRequest request) throws IOException {
        if (category == null) {
            category = new Category();
        }
        category.setName(request.getName());
        category.setLinkValue(request.getLinkValue());
        category.setMinGoodsPerOrder(request.getMinGoodsPerOrder());
        SeoInfo seoInfo = new SeoInfo();
        seoInfo.setDescription(request.getSeoInfoRequest().getDescription());
        seoInfo.setHeader(request.getSeoInfoRequest().getHeader());
        seoInfo.setTitle(request.getSeoInfoRequest().getTitle());
        seoInfo.setTextContent(request.getSeoInfoRequest().getTextContent());
        category.setSeoInfo(seoInfo);
        if (request.getImage() != null && !request.getImage().isEmpty()) {
            String imageName = fileService.saveFile(request.getImage(), imageDirectoryPath);
            category.setPathToImage(imageName);
        }
        if (request.getIcon() != null && !request.getIcon().isEmpty()) {
            String iconName = fileService.saveFile(request.getIcon(), iconDirectoryPath);
            category.setPathToIcon(iconName);
        }
        return category;
    }

    private Category findCategory(Long id) throws ObjectNotFoundException {
        Category category = categoryRepository.findOne(id);
        if (category != null) {
            return category;
        } else {
            throw new ObjectNotFoundException("Category with id :" + id + " not found");
        }
    }
}
