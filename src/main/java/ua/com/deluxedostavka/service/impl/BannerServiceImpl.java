package ua.com.deluxedostavka.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.deluxedostavka.dto.banner.BannerRequest;
import ua.com.deluxedostavka.dto.banner.BannerResponse;
import ua.com.deluxedostavka.dto.feedback.FeedbackResponse;
import ua.com.deluxedostavka.dto.other.DataResponse;
import ua.com.deluxedostavka.dto.other.PaginationRequest;
import ua.com.deluxedostavka.dto.other.SortRequest;
import ua.com.deluxedostavka.entity.Banner;
import ua.com.deluxedostavka.entity.Feedback;
import ua.com.deluxedostavka.entity.SeoInfo;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;
import ua.com.deluxedostavka.repository.BannerRepository;
import ua.com.deluxedostavka.service.BannerService;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.stream.Collectors;

@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    private BannerRepository bannerRepository;

    @Autowired
    private FileService fileService;

    @Value("${banner.img.directory}")
    private String imageDirectoryPath;

    @Override
    @Transactional(readOnly = true)
    public DataResponse<BannerResponse> getAll(PaginationRequest paginationRequest) {
        Page<Banner> page = bannerRepository.findAll(getPageRequest(paginationRequest));
        return new DataResponse<>(page.getContent().stream().map(BannerResponse::new).collect(Collectors.toList()), page.getTotalElements(), page.getTotalPages());
    }

    @Override
    @Transactional(readOnly = true)
    public BannerResponse getOne(Long id) {
        return new BannerResponse(bannerRepository.findOne(id));
    }

    @Override
    @Transactional
    public BannerResponse create(BannerRequest bannerRequest) throws IOException {
        Banner banner = bannerRepository.saveAndFlush(bannerRequestToBanner(null, bannerRequest));
        return new BannerResponse(banner);
    }

    @Override
    @Transactional
    public BannerResponse update(Long id, BannerRequest bannerRequest) throws ObjectNotFoundException, IOException {
        Banner banner = bannerRepository.saveAndFlush(bannerRequestToBanner(findBanner(id), bannerRequest));
        return new BannerResponse(banner);
    }

    @Override
    @Transactional
    public void delete(Long id) throws ObjectNotFoundException {
        Banner banner = findBanner(id);
        if (banner.getPathToImage() != null) {
            Paths.get(System.getProperty("user.home"), imageDirectoryPath, banner.getPathToImage()).toFile().delete();
        }
        bannerRepository.delete(banner);
    }


    private Banner bannerRequestToBanner(Banner banner, BannerRequest request) throws IOException {
        if (banner == null) {
            banner = new Banner();
        }
        banner.setPromotionLink(request.getPromotionLink());
        SeoInfo seoInfo = new SeoInfo();
        seoInfo.setDescription(request.getSeoInfoRequest().getDescription());
        seoInfo.setHeader(request.getSeoInfoRequest().getHeader());
        seoInfo.setTitle(request.getSeoInfoRequest().getTitle());
        seoInfo.setTextContent(request.getSeoInfoRequest().getTextContent());
        banner.setSeoInfo(seoInfo);
        if (request.getImage() != null && !request.getImage().isEmpty()) {
            String imageName = fileService.saveFile(request.getImage(), imageDirectoryPath);
            banner.setPathToImage(imageName);
        }
        return banner;
    }

    private Banner findBanner(Long id) throws ObjectNotFoundException {
        Banner banner = bannerRepository.findOne(id);
        if (banner != null) {
            return banner;
        } else {
            throw new ObjectNotFoundException("Banner with id :" + id + " not found");
        }
    }

    private PageRequest getPageRequest(PaginationRequest paginationRequest) {
        SortRequest sortRequest = paginationRequest.getSort();
        PageRequest pageRequest;
        if (sortRequest == null) {
            pageRequest = new PageRequest(paginationRequest.getPage(), paginationRequest.getSize());
        } else {
            pageRequest = new PageRequest(paginationRequest.getPage(), paginationRequest.getSize(), new Sort(sortRequest.getDirection(), sortRequest.getFieldName()));
        }
        return pageRequest;
    }
}
