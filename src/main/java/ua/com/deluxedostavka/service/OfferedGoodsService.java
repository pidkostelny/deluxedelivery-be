package ua.com.deluxedostavka.service;

import ua.com.deluxedostavka.dto.offeredGoods.OfferedGoodsRequest;

public interface OfferedGoodsService {

    void create(OfferedGoodsRequest offeredGoodsRequest);

    void delete(Long id);

    void delete(OfferedGoodsRequest offeredGoodsRequest);

}
