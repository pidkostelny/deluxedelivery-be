package ua.com.deluxedostavka.service;

import ua.com.deluxedostavka.dto.banner.BannerRequest;
import ua.com.deluxedostavka.dto.banner.BannerResponse;
import ua.com.deluxedostavka.dto.other.DataResponse;
import ua.com.deluxedostavka.dto.other.PaginationRequest;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;

import java.io.IOException;

public interface BannerService {

    DataResponse<BannerResponse> getAll(PaginationRequest paginationRequest);

    BannerResponse getOne(Long id);

    BannerResponse create(BannerRequest bannerRequest) throws IOException;

    BannerResponse update(Long id, BannerRequest bannerRequest) throws ObjectNotFoundException, IOException;

    void delete(Long id) throws ObjectNotFoundException;


}
