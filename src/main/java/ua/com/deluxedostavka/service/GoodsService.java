package ua.com.deluxedostavka.service;

import ua.com.deluxedostavka.dto.goods.FullAdminGoodsResponse;
import ua.com.deluxedostavka.dto.goods.FullGoodsResponse;
import ua.com.deluxedostavka.dto.goods.GoodsRequest;
import ua.com.deluxedostavka.dto.goods.GoodsResponse;
import ua.com.deluxedostavka.dto.other.DataResponse;
import ua.com.deluxedostavka.dto.other.PaginationRequest;
import ua.com.deluxedostavka.entity.Goods;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;

import java.io.IOException;
import java.util.List;

public interface GoodsService {

    List<GoodsResponse> getAll();

    List<GoodsResponse> getAllSales();

    DataResponse<GoodsResponse> getAll(PaginationRequest paginationRequest);

    DataResponse<GoodsResponse> getPageByCategoryId(Long id, PaginationRequest paginationRequest);

    DataResponse<GoodsResponse> getPageBySubCategoryLinkValue(String linkValue, PaginationRequest paginationRequest);

    DataResponse<GoodsResponse> getPageBySubCategoryId(Long id, PaginationRequest paginationRequest);

    DataResponse<GoodsResponse> getPageByCategoryLinkValue(String linkValue, PaginationRequest paginationRequest);

    List<GoodsResponse> getAllBySubCategory(Long idSubCategory);

    List<GoodsResponse> getAllByCategory(Long idCategory);

    GoodsResponse getOne(Long idGoods);

    FullGoodsResponse getFullOne(Long idGoods);

    FullGoodsResponse getFullOneByLinkValue(String linkValue);

    FullAdminGoodsResponse getAdminFullOne(Long idGoods);

    DataResponse<GoodsResponse> findByName(String name, PaginationRequest paginationRequest);

    GoodsResponse create(GoodsRequest goodsRequest) throws IOException, ObjectNotFoundException;

    GoodsResponse update(Long id ,GoodsRequest goodsRequest) throws ObjectNotFoundException, IOException;

    void delete(Long id) throws ObjectNotFoundException;

    Goods findOne(Long id);

}
