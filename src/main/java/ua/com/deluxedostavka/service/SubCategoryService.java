package ua.com.deluxedostavka.service;


import ua.com.deluxedostavka.dto.subCategory.SubCategoryRequest;
import ua.com.deluxedostavka.dto.subCategory.SubCategoryResponse;
import ua.com.deluxedostavka.entity.SubCategory;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;

import java.io.IOException;
import java.util.List;

public interface SubCategoryService {

    List<SubCategoryResponse> getAll();

    List<SubCategoryResponse> getAllByCategoryLinkValue(String linkValue);

    List<SubCategoryResponse> getAllByCategoryId(Long categoryId);

    SubCategoryResponse getOne(Long id);

    SubCategory findSubCategory(Long id) throws ObjectNotFoundException;

    SubCategoryResponse create(SubCategoryRequest subCategoryRequest) throws ObjectNotFoundException, IOException;

    SubCategoryResponse update(Long id ,SubCategoryRequest subCategoryRequest) throws ObjectNotFoundException, IOException;

    void delete(Long id) throws ObjectNotFoundException;

}
