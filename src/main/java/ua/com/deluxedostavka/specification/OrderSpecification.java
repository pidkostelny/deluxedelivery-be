package ua.com.deluxedostavka.specification;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import ua.com.deluxedostavka.dto.order.OrderSearchRequest;
import ua.com.deluxedostavka.dto.statistic.StatisticRequest;
import ua.com.deluxedostavka.entity.Order;
import ua.com.deluxedostavka.entity.PaymentType;
import ua.com.deluxedostavka.tools.Utils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static ua.com.deluxedostavka.tools.Constants.WILDCARD_FOR_SQL_LIKE;

@AllArgsConstructor
public class OrderSpecification implements Specification<Order> {

    private Long minSum;
    private Long maxSum;

    private LocalDate dateFrom;
    private LocalDate dateTo;

    private Boolean archived;

    private String value;

    private PaymentType paymentType;

    public OrderSpecification(OrderSearchRequest request) {
        minSum = request.getSumFrom();
        maxSum = request.getSumTo();
        archived = request.getArchived();
        value = request.getValue();
        dateFrom = Utils.dateToLocalDate(request.getDateFrom());
        dateTo = Utils.dateToLocalDate(request.getDateTo());
        paymentType = request.getPaymentType();
    }

    public OrderSpecification(StatisticRequest request) {
        dateFrom = Utils.dateToLocalDate(request.getDateFrom());
        dateTo = Utils.dateToLocalDate(request.getDateTo());
        archived = request.getArchived();
        value = request.getValue();
    }

    public Predicate toPredicate(Root<Order> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(findBySum(root, criteriaBuilder));
        predicates.add(findByDate(root, criteriaBuilder));
        predicates.add(findByArchived(root, criteriaBuilder));
        predicates.add(findByPhoneNumber(root, criteriaBuilder));
        predicates.add(findByPaymentType(root, criteriaBuilder));
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

    private Predicate findByPaymentType(Root<Order> r, CriteriaBuilder cb) {
        if (paymentType != null) {
            return cb.equal(r.get("payment_type"), paymentType);
        } else {
            return cb.conjunction();
        }
    }

    private Predicate findByArchived(Root<Order> r, CriteriaBuilder cb) {
        return archived ? cb.isTrue(r.get("archive")) : cb.isFalse(r.get("archive"));
    }

    private Predicate findBySum(Root<Order> r, CriteriaBuilder cb) {
        Predicate predicate;
        if (maxSum == null && minSum == null) {
            predicate = cb.conjunction();
        } else if (maxSum == null) {
            predicate = cb.greaterThanOrEqualTo(r.get("sum"), minSum);
        } else if (minSum == null) {
            predicate = cb.lessThanOrEqualTo(r.get("sum"), maxSum);
        } else {
            predicate = cb.between(r.get("sum"), minSum, maxSum);
        }
        return predicate;
    }

    private Predicate findByDate(Root<Order> r, CriteriaBuilder cb) {
        Predicate predicate;
        if (dateFrom == null && dateTo == null) {
            predicate = cb.conjunction();
        } else if (dateTo == null) {
            predicate = cb.greaterThanOrEqualTo(r.get("date"), dateFrom);
        } else if (dateFrom == null) {
            predicate = cb.lessThanOrEqualTo(r.get("date"), dateTo);
        } else {
            predicate = cb.between(r.get("date"), dateFrom, dateTo);
        }
        return predicate;
    }

    private Predicate findByPhoneNumber(Root<Order> r, CriteriaBuilder cb) {
        Predicate predicate;
        if (value != null && !value.trim().isEmpty()) {
            return cb.like(r.join("user").get("number"), WILDCARD_FOR_SQL_LIKE + value.trim() + WILDCARD_FOR_SQL_LIKE);
        } else {
            predicate = cb.conjunction();
        }
        return predicate;
    }

}
