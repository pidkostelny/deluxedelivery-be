package ua.com.deluxedostavka.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.nio.file.Paths;

@Configuration
@EnableWebMvc
public class StaticResourceConfiguration extends WebMvcConfigurerAdapter {

    @Value("${img.base-directory}")
    private String baseImgDirectory;

    @Value("${category.img.directory}")
    private String categoryImagesDirectoryPath;

    @Value("${category.img.icon.directory}")
    private String categoryIconsDirectoryPath;

    @Value("${subcategory.img.icon.directory}")
    private String subcategoryIconsDirectoryPath;

    @Value("${subcategory.img.directory}")
    private String subCategoryImagesDirectoryPath;

    @Value("${product.img.directory}")
    private String productImagesDirectoryPath;

    @Value("${banner.img.directory}")
    private String bannerImagesDirectoryPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        System.out.println(getPath(categoryImagesDirectoryPath));
        System.out.println(getPath(subCategoryImagesDirectoryPath));
        registry.addResourceHandler("/categoryImages/**")
                .addResourceLocations(getPath(categoryImagesDirectoryPath));
        registry.addResourceHandler("/categoryImages/icons/**")
                .addResourceLocations(getPath(categoryIconsDirectoryPath));
        registry.addResourceHandler("/subcategoryImages/**")
                .addResourceLocations(getPath(subCategoryImagesDirectoryPath));
        registry.addResourceHandler("/subcategoryImages/icons/**")
                .addResourceLocations(getPath(subcategoryIconsDirectoryPath));
        registry.addResourceHandler("/images/**")
                .addResourceLocations(getPath(productImagesDirectoryPath));
        registry.addResourceHandler("/banners/**")
                .addResourceLocations(getPath(bannerImagesDirectoryPath));
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/META-INF/resources/","classpath:/resources/","classpath:/static/","classpath:/public/","classpath:/static/admin-app/");
    }

    private String getPath(String directory) {
        return String.format("file://%s/", Paths.get(baseImgDirectory, directory));
    }
}

