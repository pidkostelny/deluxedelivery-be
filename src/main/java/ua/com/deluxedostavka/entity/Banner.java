package ua.com.deluxedostavka.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class Banner extends IdHolder {

    private String pathToImage;

    @Embedded
    private SeoInfo seoInfo;

    private String promotionLink;

}
