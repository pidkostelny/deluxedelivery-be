package ua.com.deluxedostavka.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@Entity
public class Goods extends IdHolder {

    private String name;

    private Long price;

    private Long weight;

    private String goodsDescription;

    private String pathImage;

    private Boolean sale;

    private Boolean deleted;

    @Column(unique = true)
    private String linkValue;

    @Embedded
    private SeoInfo seoInfo;

    @ManyToOne
    private SubCategory subCategory;

    @OneToMany(mappedBy = "goods")
    private Set<GoodsForOrder> goodsForOrders = new LinkedHashSet<>();

    @OneToMany(mappedBy = "goods")
    private Set<Feedback> feedbacks = new LinkedHashSet<>();

    @OneToMany(mappedBy = "mainGoods")
    private Set<OfferedGoods> offeredMainGoods = new LinkedHashSet<>();

    @OneToMany(mappedBy = "anotherGoods")
    private Set<OfferedGoods> offeredAnotherGoods = new LinkedHashSet<>();

}
