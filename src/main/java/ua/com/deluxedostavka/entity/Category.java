package ua.com.deluxedostavka.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter @Setter
@Entity
public class Category extends IdHolder{

    @Column(unique = true)
    private String name;

    @Column(unique = true)
    private String linkValue;

    @Embedded
    private SeoInfo seoInfo;

    private String pathToImage;

    private String pathToIcon;

    private Long minGoodsPerOrder;

    @OneToMany(mappedBy = "category")
    private Set<SubCategory> subCategoryList = new LinkedHashSet<>();

    @Override
    public String toString() {
        return name;
    }
}
