package ua.com.deluxedostavka.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter @Setter
@Entity
public class SubCategory extends IdHolder{

    private String name;

    private String linkValue;

    @Embedded
    private SeoInfo seoInfo;

    private String pathToImage;

    private String pathToIcon;

    @ManyToOne
    private Category category;

    @OneToMany(mappedBy = "subCategory")
    private Set<Goods> goods = new LinkedHashSet<>();

    @Override
    public String toString() {
        return name;
    }
}
