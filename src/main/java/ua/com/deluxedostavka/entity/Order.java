package ua.com.deluxedostavka.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Getter @Setter
@Entity
@Table(name = "_order")
public class Order extends IdHolder{

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate date;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalTime time;

    private Boolean archive;

    private Boolean dontCallBack;

    private Boolean dontNeedCutlery;

    private String description;

    private Long sum;

    private Long deliveryPrice;

    @Enumerated(EnumType.STRING)
    private PaymentType paymentType;

    @OneToOne(cascade = CascadeType.REMOVE)
    @PrimaryKeyJoinColumn
    private User user;

    @OneToMany(mappedBy = "order",cascade = CascadeType.REMOVE)
    private Set<GoodsForOrder> goodsForOrders = new LinkedHashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(date, order.date) &&
                Objects.equals(time, order.time) &&
                Objects.equals(archive, order.archive) &&
                Objects.equals(dontCallBack, order.dontCallBack) &&
                Objects.equals(dontNeedCutlery, order.dontNeedCutlery) &&
                Objects.equals(description, order.description) &&
                Objects.equals(sum, order.sum) &&
                Objects.equals(deliveryPrice, order.deliveryPrice) &&
                paymentType == order.paymentType &&
                Objects.equals(user, order.user) &&
                Objects.equals(goodsForOrders, order.goodsForOrders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, time, archive, dontCallBack, dontNeedCutlery, description, sum, deliveryPrice, paymentType, user, goodsForOrders);
    }
}
