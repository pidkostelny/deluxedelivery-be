package ua.com.deluxedostavka.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter@Setter
public class OfferedGoods extends IdHolder {

    @ManyToOne
    private Goods mainGoods;

    @ManyToOne
    private Goods anotherGoods;

}
