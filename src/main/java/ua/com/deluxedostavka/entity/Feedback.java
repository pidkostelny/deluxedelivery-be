package ua.com.deluxedostavka.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class Feedback extends IdHolder {

    private String name;

    private String phoneNumber;

    private Long rating;

    private String messageText;

    @ManyToOne
    private Goods goods;

}
