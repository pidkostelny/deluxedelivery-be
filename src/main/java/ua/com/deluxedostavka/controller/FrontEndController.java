package ua.com.deluxedostavka.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class FrontEndController implements ErrorController {

    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public String error(HttpServletRequest request) throws JsonProcessingException {
        final Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
        final Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == 404) {
            return "forward:/index.html";
        } else {
            return exceptionToJson(exception);
        }
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

    private String exceptionToJson(Exception e) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(e);
    }
}
