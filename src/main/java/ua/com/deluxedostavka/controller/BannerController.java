package ua.com.deluxedostavka.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.com.deluxedostavka.dto.banner.BannerRequest;
import ua.com.deluxedostavka.dto.banner.BannerResponse;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;
import ua.com.deluxedostavka.service.BannerService;

import javax.validation.Valid;
import java.io.IOException;

@CrossOrigin
@RestController
@RequestMapping("/banner")
public class BannerController {

    @Autowired
    private BannerService bannerService;

    @PostMapping
    public BannerResponse create(@RequestBody @Valid BannerRequest bannerRequest) throws IOException {
        return bannerService.create(bannerRequest);
    }

    @PutMapping("/{id}")
    public BannerResponse update(@PathVariable Long id, @RequestBody @Valid BannerRequest bannerRequest) throws ObjectNotFoundException, IOException {
        return bannerService.update(id, bannerRequest);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) throws ObjectNotFoundException {
        bannerService.delete(id);
    }

}
