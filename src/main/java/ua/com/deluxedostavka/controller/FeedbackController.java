package ua.com.deluxedostavka.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.com.deluxedostavka.dto.feedback.AdminFeedbackResponse;
import ua.com.deluxedostavka.dto.feedback.FeedbackRequest;
import ua.com.deluxedostavka.dto.feedback.FeedbackResponse;
import ua.com.deluxedostavka.dto.other.DataResponse;
import ua.com.deluxedostavka.dto.other.PaginationRequest;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;
import ua.com.deluxedostavka.service.FeedbackService;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/feedback")
public class FeedbackController {

    @Autowired
    private FeedbackService feedbackService;

    @PostMapping
    public DataResponse<AdminFeedbackResponse> getAll(@RequestParam(required = false) Long goodsId, @RequestBody PaginationRequest paginationRequest) {
        return feedbackService.getFullAll(paginationRequest, goodsId);
    }

    @GetMapping("/{id}")
    public AdminFeedbackResponse getOne(@PathVariable Long id){
        return feedbackService.getFullOne(id);
    }

    @PutMapping("/{id}")
    public FeedbackResponse update(@PathVariable Long id, @RequestBody @Valid FeedbackRequest feedbackRequest) throws ObjectNotFoundException {
        return feedbackService.update(id, feedbackRequest);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) throws ObjectNotFoundException {
        feedbackService.delete(id);
    }

}
