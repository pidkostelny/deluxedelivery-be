package ua.com.deluxedostavka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.com.deluxedostavka.dto.statistic.StatisticRequest;
import ua.com.deluxedostavka.dto.statistic.StatisticResponse;
import ua.com.deluxedostavka.service.impl.StatisticService;

@CrossOrigin
@RestController
@RequestMapping("/statistic")
public class StatisticController {

    @Autowired
    private StatisticService statisticService;

    @PostMapping
    public StatisticResponse getStatistic(@RequestBody StatisticRequest request) {
        return statisticService.getStatisticByFilter(request);
    }
}
