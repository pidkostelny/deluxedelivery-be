package ua.com.deluxedostavka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.com.deluxedostavka.dto.authentication.CredentialsRequest;
import ua.com.deluxedostavka.dto.authentication.TokenResponse;
import ua.com.deluxedostavka.dto.banner.BannerResponse;
import ua.com.deluxedostavka.dto.category.CategoryResponse;
import ua.com.deluxedostavka.dto.category.CategoryWithChildrenResponse;
import ua.com.deluxedostavka.dto.catering.CateringRequest;
import ua.com.deluxedostavka.dto.feedback.FeedbackResponse;
import ua.com.deluxedostavka.dto.goods.FullGoodsResponse;
import ua.com.deluxedostavka.dto.goods.GoodsResponse;
import ua.com.deluxedostavka.dto.order.OrderRequest;
import ua.com.deluxedostavka.dto.order.OrderResponse;
import ua.com.deluxedostavka.dto.other.DataResponse;
import ua.com.deluxedostavka.dto.other.SimpleFeedbackRequest;
import ua.com.deluxedostavka.dto.other.PaginationRequest;
import ua.com.deluxedostavka.dto.subCategory.SubCategoryResponse;
import ua.com.deluxedostavka.exception.AuthenticationException;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;
import ua.com.deluxedostavka.repository.GoodsRepository;
import ua.com.deluxedostavka.service.*;
import ua.com.deluxedostavka.tools.TelegramTool;
import ua.com.deluxedostavka.tools.Transliteration;

import javax.validation.Valid;
import javax.xml.bind.JAXBException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/public")
public class PublicController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SubCategoryService subCategoryService;

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private BannerService bannerService;

    @Autowired
    private TelegramTool telegramTool;

    @PostMapping("/authentication")
    public TokenResponse authentication(@RequestBody @Valid CredentialsRequest credentialsRequest) throws AuthenticationException {
        return administratorService.authentication(credentialsRequest);
    }

    @GetMapping("/category")
    public List<CategoryResponse> getAllCategory() {
        return categoryService.getAll();
    }

    @GetMapping("/category/tree")
    public List<CategoryWithChildrenResponse> getAllTree() {
        return categoryService.getAllTree();
    }

    @GetMapping("/subCategory")
    public List<SubCategoryResponse> getAllSubCategory() {
        return subCategoryService.getAll();
    }

    @GetMapping("/subCategory/{idCategory}")
    public List<SubCategoryResponse> getAllSubCategoryByCategory(@PathVariable Long idCategory) {
        return subCategoryService.getAllByCategoryId(idCategory);
    }

    @GetMapping("/subCategory/linkValue/{linkValue}")
    public List<SubCategoryResponse> getAllSubCategoryByCategory(@PathVariable String linkValue) {
        return subCategoryService.getAllByCategoryLinkValue(linkValue);
    }

    @GetMapping("/goods/{id}")
    public FullGoodsResponse getFullOneGoods(@PathVariable Long id) {
        return goodsService.getFullOne(id);
    }

    @GetMapping("/goods/linkValue/{linkValue}")
    public FullGoodsResponse getFullOneGoodsByLinkValue(@PathVariable String linkValue) {
        return goodsService.getFullOneByLinkValue(linkValue);
    }

    @GetMapping("/goods")
    public List<GoodsResponse> getAllGoods() {
        return goodsService.getAll();
    }

    @GetMapping("/goods/sales")
    public List<GoodsResponse> getAllSalesGoods() {
        return goodsService.getAllSales();
    }

    @PostMapping("/goods/page")
    public DataResponse<GoodsResponse> getAllGoods(@RequestBody PaginationRequest paginationRequest) {
        return goodsService.getAll(paginationRequest);
    }

    @PostMapping("/goods/page/by/category/{id}")
    public DataResponse<GoodsResponse> getPageGoodsByCategoryId(@PathVariable Long id, @RequestBody PaginationRequest paginationRequest) {
        return goodsService.getPageByCategoryId(id, paginationRequest);
    }

    @PostMapping("/goods/page/by/categoryLinkValue/{linkValue}")
    public DataResponse<GoodsResponse> getPageGoodsByCategoryName(@PathVariable String linkValue, @RequestBody PaginationRequest paginationRequest) {
        return goodsService.getPageByCategoryLinkValue(linkValue, paginationRequest);
    }

    @PostMapping("/goods/page/by/subCategory/{id}")
    public DataResponse<GoodsResponse> getPageGoodsBySubCategoryId(@PathVariable Long id, @RequestBody PaginationRequest paginationRequest) {
        return goodsService.getPageBySubCategoryId(id, paginationRequest);
    }

    @PostMapping("/goods/page/by/subCategoryLinkValue/{linkValue}")
    public DataResponse<GoodsResponse> getPageGoodsBySubCategoryLinkValue(@PathVariable String linkValue, @RequestBody PaginationRequest paginationRequest) {
        return goodsService.getPageBySubCategoryLinkValue(linkValue, paginationRequest);
    }

    @PostMapping("/goods/search")
    public DataResponse<GoodsResponse> getAllGoodsByName(@RequestParam String name, @RequestBody PaginationRequest paginationRequest) {
        return goodsService.findByName(name, paginationRequest);
    }

    @GetMapping("/goods/by/category/{idCategory}")
    public List<GoodsResponse> getAllGoodsByCategory(@PathVariable Long idCategory) {
        return goodsService.getAllByCategory(idCategory);
    }

    @GetMapping("/goods/by/subCategory/{idSubCategory}")
    public List<GoodsResponse> getAllGoodsBySubCategory(@PathVariable Long idSubCategory) {
        return goodsService.getAllBySubCategory(idSubCategory);
    }

    @PostMapping("/order")
    public OrderResponse order(@RequestBody @Valid OrderRequest orderRequest) throws ObjectNotFoundException, JAXBException {
        return orderService.order(orderRequest);
    }

    @PostMapping("/goods/feedback")
    public FeedbackResponse createFeedback(@RequestBody @Valid ua.com.deluxedostavka.dto.feedback.FeedbackRequest feedbackRequest) {
        return feedbackService.create(feedbackRequest);
    }

    @PostMapping("/goods/feedback/all")
    public DataResponse<FeedbackResponse> getAllFeedbacks(@RequestParam(required = false) Long goodsId, @RequestBody PaginationRequest paginationRequest) {
        return feedbackService.getAll(paginationRequest, goodsId);
    }

    @GetMapping("/goods/feedback/{id}")
    public FeedbackResponse getOneFeedback(@PathVariable Long id) {
        return feedbackService.getOne(id);
    }

    @PostMapping("/feedback")
    public void sendFeedbackNotification(@RequestBody SimpleFeedbackRequest simpleFeedbackRequest) {
        telegramTool.sendFeedbackNotification(simpleFeedbackRequest);
    }

    @PostMapping("/catering")
    public void sendCateringNotification(@RequestBody CateringRequest cateringRequest) {
        telegramTool.sendCateringNotification(cateringRequest);
    }

    @PostMapping("/banner/all")
    public DataResponse<BannerResponse> getAllBanners(@RequestBody PaginationRequest paginationRequest) {
        return bannerService.getAll(paginationRequest);
    }

    @GetMapping("/banner/{id}")
    public BannerResponse getOneBanner(@PathVariable Long id) {
        return bannerService.getOne(id);
    }

    @Autowired
    private GoodsRepository goodsRepository;

    @GetMapping("/generate/lonk/value")
    public void generate() {
        goodsRepository.findAll()
                .forEach(goods -> {
                    goods.setLinkValue(Transliteration.transliterateForUrl(goods.getName()));
                    goodsRepository.save(goods);
                });

    }

}
