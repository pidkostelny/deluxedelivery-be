package ua.com.deluxedostavka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ua.com.deluxedostavka.dto.offeredGoods.OfferedGoodsRequest;
import ua.com.deluxedostavka.exception.ObjectNotFoundException;
import ua.com.deluxedostavka.service.OfferedGoodsService;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/offeredGoods")
public class OfferedGoodsController {

    @Autowired
    private OfferedGoodsService offeredGoodsService;

    @PostMapping
    public void create(@RequestBody @Valid OfferedGoodsRequest offeredGoodsRequest) throws ObjectNotFoundException {
        offeredGoodsService.create(offeredGoodsRequest);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody @Valid OfferedGoodsRequest offeredGoodsRequest) throws ObjectNotFoundException {
        offeredGoodsService.delete(offeredGoodsRequest);
    }
}
