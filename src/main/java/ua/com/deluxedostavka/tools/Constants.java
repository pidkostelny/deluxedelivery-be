package ua.com.deluxedostavka.tools;

public class Constants {

    public static final String WILDCARD_FOR_SQL_LIKE = "%";

    public static final String KIEV_ZONE = "Europe/Kiev";

    public static final String TELEGRAM_ORDERS_URL = "https://api.telegram.org/bot650088115:AAH5P9G_WZjT5swP9KQsMfO3d9taajc5uxI/sendMessage?chat_id=-1001208922424&text=";

    public static final String TELEGRAM_FEEDBACK_URL = "https://api.telegram.org/bot811551926:AAHmwGEAX1piLSMUGBJYcmdh51BOiBQfQPk/sendMessage?chat_id=-273373533&text=";

    public static final String TELEGRAM_CATERING_URL = "https://api.telegram.org/bot883416985:AAFqrryV2cpGUiY6AcnsKdwys0YUDJ5ESxo/sendMessage?chat_id=372589403&text=";

    public static final String FEEDBACK_TEMPLATE = "Зворотній зв'язок:%nКонтакт: %s%nВідгук: %s";

    public static final String CATERING_TEMPLATE = "Замовлення:%nДата заходу: %s%nКількість гостей: %s%nТип заходу: %s%nФормат обслуговування: %s%nТривалість заходу: %s%nБюджет: %s%nЗамовник%nІм'я: %s%nКомпанія: %s%nНомер: +380%s%nEmail: %s%nКоментар: %s%n";

}
