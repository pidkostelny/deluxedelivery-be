package ua.com.deluxedostavka.tools;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ua.com.deluxedostavka.dto.catering.CateringRequest;
import ua.com.deluxedostavka.dto.other.SimpleFeedbackRequest;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static ua.com.deluxedostavka.tools.Constants.*;

@Component
@Log4j2
public class TelegramTool {

    public void sendNotification(String url){
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(url, String.class);
        log.info("Telegram response -> "+response);
    }

    public void sendFeedbackNotification(SimpleFeedbackRequest simpleFeedbackRequest) {
        String message = String.format(FEEDBACK_TEMPLATE, simpleFeedbackRequest.getUser(), simpleFeedbackRequest.getText());
        sendNotification(TELEGRAM_FEEDBACK_URL + message);
    }

    public void sendCateringNotification(CateringRequest request) {
        String message = String.format(CATERING_TEMPLATE,
                request.getInfo().getDate().toInstant().atZone(ZoneId.of(KIEV_ZONE)).format(DateTimeFormatter.ofPattern("cccc - dd MMMM yyyyр.", Locale.forLanguageTag("uk"))),
                request.getInfo().getGuests(),
                request.getInfo().getType(),
                request.getInfo().getFormat(),
                request.getInfo().getDuration(),
                request.getInfo().getBudget(),
                request.getPerson().getName(),
                request.getPerson().getCompany(),
                request.getPerson().getPhoneNumber(),
                request.getPerson().getEmail(),
                request.getPerson().getComment());
        sendNotification(TELEGRAM_CATERING_URL + message);
    }

}
