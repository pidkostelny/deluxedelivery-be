package ua.com.deluxedostavka.tools;

import java.util.Arrays;
import java.util.List;

public class Transliteration {

    public static String transliterateForUrl(String message){
        message = message.toLowerCase();
        message = message.replaceAll(" - ", "-");
        message = message.replaceAll("\\.", "");
        message = message.replaceAll("/", "");
        message = message.replaceAll(" {2}", "-");
        message = message.replaceAll(" ", "-");
        message = message.replaceAll("#", "");
        message = message.replaceAll("'", "");
        return transliterate(message);
    }

    public static String transliterate(String message) {
        List<Character> abcCyr = Arrays.asList('а', 'б', 'в', 'ґ', 'д', 'е', 'ж', 'з', 'и', 'і', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ь', 'А', 'Б', 'В', 'Ґ', 'Д', 'Е', 'Ж', 'З', 'И', 'І', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ь');
        List<String> abcLat = Arrays.asList("a", "b", "v", "g", "d", "e", "zh", "z", "y", "i", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "kh", "ts", "ch", "sh", "sch", "", "A", "B", "V", "G", "D", "E", "Zh", "Z", "Y", "I", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "Kh", "Ts", "Ch", "Sh", "Sch", "");

        List<Character> specialAbcCyr = Arrays.asList('є', 'ї', 'й', 'ю', 'я', 'Є', 'Ї', 'Й', 'Ю', 'Я');
        List<String> beginAbcLat = Arrays.asList("ye", "yi", "y", "yu", "ya", "Ye", "Yi", "Y", "Yu", "Ya");
        List<String> middleAbcLat = Arrays.asList("ie", "i", "i", "iu", "ia", "Ie", "I", "I", "Iu", "Ia");

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            Character tempChar = message.charAt(i);
            if (abcCyr.contains(tempChar)) {
                builder.append(abcLat.get(abcCyr.indexOf(tempChar)));
            }else if (abcLat.contains(tempChar.toString())){
                builder.append(tempChar);
            }else {
                switch (tempChar) {
                    case 'г': {
                        if (i > 0 && (message.charAt(i - 1) == 'з' || message.charAt(i - 1) == 'З')) {
                            builder.append("gh");
                        } else {
                            builder.append("h");
                        }
                        break;
                    }
                    case 'Г': {
                        if (i > 0 && (message.charAt(i - 1) == 'з' || message.charAt(i - 1) == 'З')) {
                            builder.append("Gh");
                        } else {
                            builder.append("H");
                        }
                        break;
                    }
                    case 'є':
                    case 'ї':
                    case 'й':
                    case 'ю':
                    case 'я':
                    case 'Є':
                    case 'Ї':
                    case 'Й':
                    case 'Ю':
                    case 'Я': {
                        if (i == 0 || (!abcCyr.contains(message.charAt(i - 1)) && !specialAbcCyr.contains(message.charAt(i - 1)))) {
                            builder.append(beginAbcLat.get(specialAbcCyr.indexOf(tempChar)));
                        } else {
                            builder.append(middleAbcLat.get(specialAbcCyr.indexOf(tempChar)));
                        }
                        break;
                    }
                    case '-':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case '0':{
                        builder.append(tempChar);
                    }

//                    default: {
//                        builder.append(tempChar);
//                    }
                }
            }
        }
        return builder.toString();
    }

}
