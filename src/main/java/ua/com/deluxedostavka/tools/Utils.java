package ua.com.deluxedostavka.tools;

import javax.annotation.Nullable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static ua.com.deluxedostavka.tools.Constants.KIEV_ZONE;

public class Utils {

    public static LocalDate dateToLocalDate(@Nullable Date date) {
        return date == null ? null : date.toInstant().atZone(ZoneId.of(KIEV_ZONE)).toLocalDate();
    }
}
