package ua.com.deluxedostavka.dto.other;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimpleFeedbackRequest {
    private String user;
    private String text;
}
