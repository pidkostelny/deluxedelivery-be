package ua.com.deluxedostavka.dto.other;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;

@Getter @Setter
public class PaginationRequest {

    private Integer page;

    private Integer size;

    private SortRequest sort;

    public PageRequest toPageRequest() {
        if (sort != null) {
            return new PageRequest(page, size, sort.getDirection(), sort.getFieldName());
        } else {
            return new PageRequest(page, size);
        }
    }
}
