package ua.com.deluxedostavka.dto.statistic;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class StatisticRequest {
    private String value;
    private Date dateFrom;
    private Date dateTo;
    private Boolean archived;
}
