package ua.com.deluxedostavka.dto.statistic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class StatisticResponse {
    private Long totalSum;
    private Long avgSum;
    private Integer total;

}
