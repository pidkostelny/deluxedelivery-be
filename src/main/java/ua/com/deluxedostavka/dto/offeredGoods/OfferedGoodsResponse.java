package ua.com.deluxedostavka.dto.offeredGoods;

import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.goods.GoodsResponse;
import ua.com.deluxedostavka.entity.OfferedGoods;

@Getter
@Setter
public class OfferedGoodsResponse {

    private Long id;

    private GoodsResponse mainGoods;

    private GoodsResponse anotherGoods;

    public OfferedGoodsResponse(OfferedGoods offeredGoods) {
        this.id = offeredGoods.getId();
        this.mainGoods = new GoodsResponse(offeredGoods.getMainGoods());
        this.anotherGoods = new GoodsResponse(offeredGoods.getAnotherGoods());
    }
}
