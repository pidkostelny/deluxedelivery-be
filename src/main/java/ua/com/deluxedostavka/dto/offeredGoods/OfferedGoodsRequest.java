package ua.com.deluxedostavka.dto.offeredGoods;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter@Setter
public class OfferedGoodsRequest {

    @NotNull
    private Long mainGoodsId;

    @NotNull
    private Long anotherGoodsId;

}
