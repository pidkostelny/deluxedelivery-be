package ua.com.deluxedostavka.dto.catering;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CateringPersonRequest {
    private String name;
    private String company;
    private String phoneNumber;
    private String email;
    private String comment;
}
