package ua.com.deluxedostavka.dto.catering;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CateringRequest {
    private CateringInfoRequest info;
    private CateringPersonRequest person;
}
