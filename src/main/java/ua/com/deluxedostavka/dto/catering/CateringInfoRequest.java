package ua.com.deluxedostavka.dto.catering;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

@Getter
@Setter
public class CateringInfoRequest {
    private String type;
    private String format;
    private Date date;
    private String guests;
    private String duration;
    private String budget;
}
