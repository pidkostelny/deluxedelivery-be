package ua.com.deluxedostavka.dto.subCategory;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.seo.SeoInfoRequest;

import javax.validation.constraints.NotNull;

@Getter @Setter
public class SubCategoryRequest {

    @NotNull
    private String name;

    @NotNull
    private String linkValue;

    private String image;

    private String icon;

    @JsonProperty("seoInfo")
    private SeoInfoRequest seoInfoRequest;

    @NotNull
    private Long categoryId;
}
