package ua.com.deluxedostavka.dto.subCategory;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.category.CategoryResponse;
import ua.com.deluxedostavka.dto.seo.SeoInfoResponse;
import ua.com.deluxedostavka.entity.SubCategory;

@Getter
@Setter
public class SubCategoryResponse {

    private Long id;

    private String name;

    private String linkValue;

    private String pathToImage;

    private String pathToIcon;

    @JsonProperty("seoInfo")
    private SeoInfoResponse seoInfoResponse;

    private CategoryResponse categoryResponse;

    public SubCategoryResponse(SubCategory subCategory) {
        this.id = subCategory.getId();
        this.name = subCategory.getName();
        this.linkValue = subCategory.getLinkValue();
        this.pathToImage = subCategory.getPathToImage();
        this.pathToIcon = subCategory.getPathToIcon();
        this.categoryResponse = new CategoryResponse(subCategory.getCategory());
        this.seoInfoResponse = new SeoInfoResponse(subCategory.getSeoInfo());
    }

    public SubCategoryResponse(SubCategory subCategory, Boolean withParent) {
        this(subCategory);
        if (!withParent) {
            this.categoryResponse = null;
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
