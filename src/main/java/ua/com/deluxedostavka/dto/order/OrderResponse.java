package ua.com.deluxedostavka.dto.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import ua.com.deluxedostavka.dto.goods.GoodsForOrderResponse;
import ua.com.deluxedostavka.dto.user.UserResponse;
import ua.com.deluxedostavka.entity.Order;
import ua.com.deluxedostavka.entity.PaymentType;

import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static ua.com.deluxedostavka.tools.Constants.KIEV_ZONE;

@Getter
@Setter
@Log
public class OrderResponse {

    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private ZonedDateTime date;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private ZonedDateTime time;

    private Boolean archive;

    private Boolean dontCallBack;

    private Boolean dontNeedCutlery;

    private String description;

    private Long sum;

    private Long deliveryPrice;

    private PaymentType paymentType;

    private UserResponse userResponse;

    private List<GoodsForOrderResponse> goodsResponseList = new ArrayList<>();


    public OrderResponse(Order order) {
        this.id = order.getId();
        this.date = ZonedDateTime.of(order.getDate(), order.getTime(), ZoneId.of(KIEV_ZONE));
        this.time = this.date;
        this.archive = order.getArchive();
        this.dontCallBack = order.getDontCallBack();
        this.dontNeedCutlery = order.getDontNeedCutlery();
        this.sum = order.getSum();
        this.deliveryPrice = order.getDeliveryPrice();
        this.description = order.getDescription();
        this.paymentType = order.getPaymentType() != null ? order.getPaymentType() : PaymentType.CASH;
        this.userResponse = new UserResponse(order.getUser());
        goodsResponseList.addAll(order.getGoodsForOrders().stream().map(GoodsForOrderResponse::new).collect(Collectors.toList()));
    }
}
