package ua.com.deluxedostavka.dto.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.other.PaginationRequest;
import ua.com.deluxedostavka.entity.PaymentType;

import java.util.Date;

@Getter
@Setter
public class OrderSearchRequest {
    private String value;
    private Date dateFrom;
    private Date dateTo;
    private Boolean archived;
    private Long sumFrom;
    private Long sumTo;
    private PaymentType paymentType;
    @JsonProperty("pagination")
    private PaginationRequest paginationRequest;

}
