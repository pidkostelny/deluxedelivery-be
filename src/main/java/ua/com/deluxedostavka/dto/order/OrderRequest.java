package ua.com.deluxedostavka.dto.order;

import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.goods.GoodsForOrderRequest;
import ua.com.deluxedostavka.dto.user.UserRequest;
import ua.com.deluxedostavka.entity.PaymentType;

import java.util.List;

@Getter @Setter
public class OrderRequest {

    private Long id;

    private Boolean archive;

    private Boolean dontCallBack;

    private Boolean dontNeedCutlery;

    private String description;

    private Long sum;

    private Long deliveryPrice;

    private PaymentType paymentType;

    private UserRequest user;

    private List<GoodsForOrderRequest> goods;

}
