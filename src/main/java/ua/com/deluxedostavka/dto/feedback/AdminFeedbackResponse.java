package ua.com.deluxedostavka.dto.feedback;

import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.goods.GoodsResponse;
import ua.com.deluxedostavka.entity.Feedback;

@Getter
@Setter
public class AdminFeedbackResponse extends FeedbackResponse {

    private GoodsResponse goodsResponse;

    private String phoneNumber;

    public AdminFeedbackResponse(Feedback feedback) {
        super(feedback);
        this.phoneNumber = feedback.getPhoneNumber();
        this.goodsResponse = new GoodsResponse(feedback.getGoods());
    }
}
