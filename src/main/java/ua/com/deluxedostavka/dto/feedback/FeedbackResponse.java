package ua.com.deluxedostavka.dto.feedback;

import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.entity.Feedback;

@Getter
@Setter
public class FeedbackResponse {

    private Long id;

    private String name;

    private Long rating;

    private String message;

    public FeedbackResponse(Feedback feedback) {
        this.id = feedback.getId();
        this.name = feedback.getName();
        this.rating = feedback.getRating();
        this.message = feedback.getMessageText();
    }
}
