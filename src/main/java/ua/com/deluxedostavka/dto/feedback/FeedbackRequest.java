package ua.com.deluxedostavka.dto.feedback;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class FeedbackRequest {

    @NotEmpty
    private String name;

    @NotEmpty
    private String phoneNumber;

    private Long rating;

    @NotEmpty
    private String message;

    @NotNull
    private Long goodsId;

}
