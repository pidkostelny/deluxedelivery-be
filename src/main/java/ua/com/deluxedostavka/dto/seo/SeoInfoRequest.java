package ua.com.deluxedostavka.dto.seo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SeoInfoRequest {
    private String title;
    private String description;
    private String header;
    private String textContent;
}
