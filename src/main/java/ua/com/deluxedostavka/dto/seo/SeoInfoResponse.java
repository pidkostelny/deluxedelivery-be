package ua.com.deluxedostavka.dto.seo;


import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.entity.SeoInfo;

@Getter
@Setter
public class SeoInfoResponse {
    private String title;
    private String description;
    private String header;
    private String textContent;


    public SeoInfoResponse(SeoInfo seoInfo) {
        if (seoInfo != null) {
            this.title = seoInfo.getTitle();
            this.description = seoInfo.getDescription();
            this.header = seoInfo.getHeader();
            this.textContent = seoInfo.getTextContent();
        }
    }
}
