package ua.com.deluxedostavka.dto.category;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.seo.SeoInfoRequest;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Setter @Getter
public class CategoryRequest {

    @NotNull
    private String name;


    private String image;

    private String icon;

    @Min(0)
    private Long minGoodsPerOrder;

    @NotNull
    private String linkValue;

    @JsonProperty("seoInfo")
    private SeoInfoRequest seoInfoRequest;

}
