package ua.com.deluxedostavka.dto.category;

import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.subCategory.SubCategoryResponse;
import ua.com.deluxedostavka.entity.Category;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class CategoryWithChildrenResponse extends CategoryResponse {

    private List<SubCategoryResponse> children;

    public CategoryWithChildrenResponse(Category category) {
        super(category);
        this.children = category.getSubCategoryList()
                .stream()
                .map(subCategory -> new SubCategoryResponse(subCategory, false))
                .collect(Collectors.toList());
    }
}
