package ua.com.deluxedostavka.dto.category;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.seo.SeoInfoResponse;
import ua.com.deluxedostavka.entity.Category;

@Getter@Setter
public class CategoryResponse {

    private Long id;

    private String name;

    private String linkValue;

    @JsonProperty("seoInfo")
    private SeoInfoResponse seoInfoResponse;

    private String pathToImage;

    private String pathToIcon;

    private Long minGoodsPerOrder;

    public CategoryResponse(Category category) {
        this.id = category.getId();
        this.name = category.getName();
        this.linkValue = category.getLinkValue();
        this.pathToImage = category.getPathToImage();
        this.pathToIcon = category.getPathToIcon();
        this.minGoodsPerOrder = category.getMinGoodsPerOrder();
        this.seoInfoResponse = new SeoInfoResponse(category.getSeoInfo());
    }

    @Override
    public String toString() {
        return name;
    }
}
