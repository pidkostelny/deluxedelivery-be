package ua.com.deluxedostavka.dto.goods;

import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.feedback.FeedbackResponse;
import ua.com.deluxedostavka.entity.Goods;
import ua.com.deluxedostavka.entity.OfferedGoods;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class FullGoodsResponse extends GoodsResponse {

    private List<FeedbackResponse> feedbackResponses;

    private List<GoodsResponse> offeredGoodsResponses;

    public FullGoodsResponse(Goods goods) {
        super(goods);
        this.feedbackResponses = goods.getFeedbacks().stream().map(FeedbackResponse::new).collect(Collectors.toList());
        this.offeredGoodsResponses = goods.getOfferedMainGoods()
                .stream()
                .map(OfferedGoods::getAnotherGoods)
                .map(GoodsResponse::new)
                .collect(Collectors.toList());
    }
}
