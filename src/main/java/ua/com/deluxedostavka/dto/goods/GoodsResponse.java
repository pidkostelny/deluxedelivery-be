package ua.com.deluxedostavka.dto.goods;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.seo.SeoInfoResponse;
import ua.com.deluxedostavka.dto.subCategory.SubCategoryResponse;
import ua.com.deluxedostavka.entity.Goods;

@Getter
@Setter
public class GoodsResponse {

    private Long id;

    private String name;

    private Long price;

    private Long weight;

    private String description;

    private String pathImage;

    private Boolean sale;

    private SubCategoryResponse subCategoryResponse;

    private String linkValue;

    @JsonProperty("seoInfo")
    private SeoInfoResponse seoInfoResponse;

    public GoodsResponse(Goods goods) {
        this.id = goods.getId();
        this.name = goods.getName();
        this.price = goods.getPrice();
        this.weight = goods.getWeight();
        this.description = goods.getGoodsDescription();
        this.pathImage = goods.getPathImage();
        this.linkValue = goods.getLinkValue();
        this.seoInfoResponse = new SeoInfoResponse(goods.getSeoInfo());
        this.sale = goods.getSale();
        if (sale == null || !sale) {
            this.subCategoryResponse = new SubCategoryResponse(goods.getSubCategory());
        }
    }
}
