package ua.com.deluxedostavka.dto.goods;

import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.feedback.AdminFeedbackResponse;
import ua.com.deluxedostavka.dto.offeredGoods.OfferedGoodsResponse;
import ua.com.deluxedostavka.entity.Goods;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class FullAdminGoodsResponse extends GoodsResponse {

    private List<AdminFeedbackResponse> feedbackResponses;

    private List<OfferedGoodsResponse> offeredGoodsResponses;

    public FullAdminGoodsResponse(Goods goods) {
        super(goods);
        this.feedbackResponses = goods.getFeedbacks().stream().map(AdminFeedbackResponse::new).collect(Collectors.toList());
        this.offeredGoodsResponses = goods.getOfferedMainGoods().stream().map(OfferedGoodsResponse::new).collect(Collectors.toList());
    }

}
