package ua.com.deluxedostavka.dto.goods;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.seo.SeoInfoRequest;

import javax.validation.constraints.NotNull;

@Getter @Setter
public class GoodsRequest {

    private String name;

    private Long price;

    private Long weight;

    private String description;

    private String img;

    private Long  subCategoryId;

    private Boolean sale;

    @JsonProperty("seoInfo")
    private SeoInfoRequest seoInfoRequest;

}
