package ua.com.deluxedostavka.dto.banner;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.seo.SeoInfoRequest;

@Getter
@Setter
public class BannerRequest {

    private String image;

    @JsonProperty("seoInfo")
    private SeoInfoRequest seoInfoRequest;

    private String promotionLink;

}
