package ua.com.deluxedostavka.dto.banner;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import ua.com.deluxedostavka.dto.seo.SeoInfoResponse;
import ua.com.deluxedostavka.entity.Banner;

@Getter
@Setter
public class BannerResponse {

    private Long id;

    private String promotionLink;

    @JsonProperty("seoInfo")
    private SeoInfoResponse seoInfoResponse;

    private String pathToImage;

    public BannerResponse(Banner banner) {
        this.id = banner.getId();
        this.promotionLink = banner.getPromotionLink();
        this.seoInfoResponse = new SeoInfoResponse(banner.getSeoInfo());
        this.pathToImage = banner.getPathToImage();
    }
}
