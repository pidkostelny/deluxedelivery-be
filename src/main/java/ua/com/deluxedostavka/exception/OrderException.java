package ua.com.deluxedostavka.exception;

public class OrderException extends RuntimeException{
    public OrderException(String message) {
        super(message);
    }
}
